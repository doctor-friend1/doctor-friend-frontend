import Navigation from "../components/landing-page/Navigation";
import Header from "../components/landing-page/Header";
import Features from "../components/landing-page/Features";
import About from "../components/landing-page/About";
import Services from "../components/landing-page/Services";
import Testimonials from "../components/landing-page/Testimonials";
import Team from "../components/landing-page/Team";
import Contact from "../components/landing-page/Contact";
import JsonData from "../data/data.json";
import React, {useEffect, useState} from "react";
import SmoothScroll from "smooth-scroll";

export const scroll = new SmoothScroll('a[href*="#"]', {
    speed: 1000,
    speedAsDuration: true,
});

const Home = () => {
    const [landingPageData, setLandingPageData] = useState({});

    useEffect(() => {
        setLandingPageData(JsonData);
    }, []);

    return (
        <>
            <Navigation/>
            <Header data={landingPageData.Header}/>
            <Features data={landingPageData.Features}/>
            <About data={landingPageData.About}/>
            <Services data={landingPageData.Services}/>
            <Testimonials data={landingPageData.Testimonials}/>
            <Team data={landingPageData.Team}/>
            <Contact data={landingPageData.Contact}/>
        </>
    )
}

export default Home;
